package archlinux

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

var aField = []byte(`%AKEY%
a value
another value

`)

var notAValue = []byte(`definitly not a field`)

func TestParseFieldSuccess(t *testing.T) {
	key, value, err := parseField(aField)
	if err != nil {
		t.Errorf("unexpected error: %s\n", err)
	}
	if key != "AKEY" {
		t.Errorf("unexpected key: %s\n", key)
	}
	fieldValue := []byte(`
a value
another value

`)
	if !cmp.Equal(value, fieldValue) {
		t.Errorf("unexpected value: %s", value)
	}
}

func TestParseFieldError(t *testing.T) {
	_, _, err := parseField(notAValue)
	switch err.(type) {
	case ErrNotParsable:
		if err.(ErrNotParsable).Reason != "unexpected data" {
			t.Errorf("unexpected reason: %s", err.(ErrNotParsable).Reason)
		}
	default:
		t.Errorf("unexpected error value: %s\n", err)
	}
}
