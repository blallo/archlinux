package archlinux

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/mikkeloscar/gopkgbuild"
)

func newDependency(name string) *Dep {
	dep := &pkgbuild.Dependency{
		Name:   name,
		MinVer: &pkgbuild.CompleteVersion{},
		MaxVer: &pkgbuild.CompleteVersion{},
	}
	return &Dep{
		Name:   name,
		Pkg:    dep,
		IsVirt: false,
	}
}

var dep1 = newDependency("pkg1")
var dep2 = newDependency("pkg2")
var dep3 = newDependency("pkg3")
var dep4 = newDependency("pkg4")
var dep5 = newDependency("pkg5")
var dep6 = newDependency("pkg6")

var tree = &DepTree{
	RootPkg:   "pkg0",
	BuildDeps: DepList{},
	Deps: DepList{
		{
			Pkg:       dep1,
			NeededBy:  nil,
			Deps:      DepList{},
			BuildDeps: DepList{},
		},
		{
			Pkg:      dep2,
			NeededBy: nil,
			Deps: DepList{
				{
					Pkg:       dep3,
					NeededBy:  dep2.Pkg,
					Deps:      DepList{},
					BuildDeps: DepList{},
				},
				{
					Pkg:       dep4,
					NeededBy:  dep2.Pkg,
					Deps:      DepList{},
					BuildDeps: DepList{},
				},
				{
					Pkg:      dep5,
					NeededBy: dep2.Pkg,
					Deps:     DepList{},
					BuildDeps: DepList{
						{
							Pkg:       dep6,
							NeededBy:  dep5.Pkg,
							Deps:      DepList{},
							BuildDeps: DepList{},
						},
					},
				},
			},
			BuildDeps: DepList{
				{
					Pkg:       dep6,
					NeededBy:  dep2.Pkg,
					Deps:      DepList{},
					BuildDeps: DepList{},
				},
			},
		},
	},
}

var deps DepList = append(tree.Deps, DepList{
	{
		Pkg:       dep3,
		NeededBy:  dep2.Pkg,
		Deps:      DepList{},
		BuildDeps: DepList{},
	},
	{
		Pkg:       dep4,
		NeededBy:  dep2.Pkg,
		Deps:      DepList{},
		BuildDeps: DepList{},
	},
	{
		Pkg:      dep5,
		NeededBy: dep2.Pkg,
		Deps:     DepList{},
		BuildDeps: DepList{
			{
				Pkg:       dep6,
				NeededBy:  dep5.Pkg,
				Deps:      DepList{},
				BuildDeps: DepList{},
			},
		},
	},
}...)

var fullDeps DepList = append(deps,
	&DepNode{
		Pkg:       dep6,
		NeededBy:  dep2.Pkg,
		Deps:      DepList{},
		BuildDeps: DepList{},
	},
)

func TestFlattenDeps(t *testing.T) {
	flattened := tree.FlattenDeps()
	opt := cmp.AllowUnexported(pkgbuild.Dependency{})
	if !cmp.Equal(flattened, deps, opt) {
		t.Errorf("Unexpected result: %+v\n", cmp.Diff(deps, flattened, opt))
	}
}

func TestFlattenAll(t *testing.T) {
	flattened := tree.FlattenAll()
	opt := cmp.AllowUnexported(pkgbuild.Dependency{})
	if !cmp.Equal(flattened, fullDeps, opt) {
		t.Errorf("Unexpected result: %+v\n", cmp.Diff(fullDeps, flattened, opt))
	}
}
