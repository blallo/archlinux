package archlinux

import (
	"compress/gzip"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var ErrNotUnsigned = errors.New("not unsigned int")
var ErrOutOfBounds = errors.New("out of bounds")

type ErrNotParsable struct {
	Key    string
	Data   []byte
	Reason string
}

func (e ErrNotParsable) Error() string {
	if e.Key != "" {
		return fmt.Sprintf("not parsable: key -> %s (reason: %s)", e.Key, e.Reason)
	}
	return fmt.Sprintf("not parsable: data -> %s (reason: %s)", e.Data, e.Reason)
}

type ErrUnknownKey struct {
	Key string
}

func (e ErrUnknownKey) Error() string {
	return fmt.Sprintf("unknown string: %s", e.Key)
}

type Blob []byte

type Entry struct {
	Blob
	FileName     string
	Name         string
	Base         string
	Groups       []string
	Version      string
	Description  string
	Csize        uint
	Isize        uint
	Md5          string
	Sha256       string
	PgpSig       string
	Url          string
	Licence      []string
	Arch         []string
	BuildDate    uint
	Packager     []string
	Replaces     []string
	Conflicts    []string
	Provides     []string
	Depends      []string
	OptDepends   []string
	MakeDepends  []string
	CheckDepends []string
}

func (e *Entry) String() string {
	return e.Name
}

type DB struct {
	Name       string
	LastUpdate *time.Time
	Data       []*Entry
}

func (db *DB) String() string {
	res := fmt.Sprintf("DB{Name: %s, Data: [", db.Name)
	for _, entry := range db.Data {
		res += fmt.Sprintf("%s, ", entry)
	}
	res = strings.TrimRight(res, ", ")
	return fmt.Sprintf("%s]}", res)
}

func getName(path string) string {
	name := filepath.Base(path)
	return strings.TrimRight(name, ".db")
}

func ParseFile(path string) (*DB, error) {
	name := getName(path)
	var db = &DB{Name: name}
	fp, err := os.Open(path)
	if err != nil {
		return db, err
	}
	defer fp.Close()

	fz, err := gzip.NewReader(fp)
	if err != nil {
		return db, err
	}

	data, err := ioutil.ReadAll(fz)
	if err != nil {
		return &DB{}, err
	}
	return parseBlob(data, db)
}

func parseBlob(data []byte, db *DB) (*DB, error) {
	var blob Blob
	var last int
	entry := &Entry{}
	for i, v := range data {
		if v == 0x25 {
			blob = data[:i]
			last = i
			break
		}
	}
	if blob == nil {
		return db, nil
	}
	entry.Blob = blob
	return parseBody(data[last:], entry, db)
}

func parseBody(data []byte, entry *Entry, db *DB) (*DB, error) {
	var start, end int
	for i, v := range data {
		if v == 0x0a && data[i+1] == 0x0a && data[i+2] == 0x25 {
			err := assignKey(data[start:i], entry)
			if err != nil {
				return db, err
			}
			start = i + 2
			continue
		}
		if v == 0x0a && data[i+1] == 0x0a && data[i+2] == 0x00 && data[i+3] == 0x00 {
			err := assignKey(data[start:i], entry)
			if err != nil {
				return db, err
			}
			end = i
			break
		}
	}
	db.Data = append(db.Data, entry)
	return parseBlob(data[end:], db)
}

func parseKeys(data []byte, entry *Entry) (int, error) {
	var start, end int
	for i, v := range data {
		if v == 0x0a && data[i+1] == 0x0a && data[i+2] == 0x25 {
			err := assignKey(data[start:i], entry)
			if err != nil {
				return 0, err
			}
			start = i
			continue
		}
		if v == 0x0a && data[i+1] == 0x0a && data[i+2] == 0x00 && data[i+3] == 0x00 {
			err := assignKey(data[start:i], entry)
			if err != nil {
				return 0, err
			}
			for j := i; true; j += 1 {
				if data[i+j] == 0x25 {
					end = i + j
					break
				}
			}
			break
		}
	}
	return end, nil
}

func assignKey(data []byte, entry *Entry) error {
	key, value, err := parseField(data)
	if err != nil {
		return err
	}
	switch key {
	case "FILENAME":
		entry.FileName, err = parseStr(value)
		if err != nil {
			return err
		}
	case "NAME":
		entry.Name, err = parseStr(value)
		if err != nil {
			return err
		}
	case "BASE":
		entry.Name, err = parseStr(value)
		if err != nil {
			return err
		}
	case "GROUPS":
		entry.Groups, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "VERSION":
		entry.Version, err = parseStr(value)
		if err != nil {
			return err
		}
	case "DESC":
		entry.Description, err = parseStr(value)
		if err != nil {
			return err
		}
	case "CSIZE":
		entry.Csize, err = parseUint(value)
		if err != nil {
			return err
		}
	case "ISIZE":
		entry.Isize, err = parseUint(value)
		if err != nil {
			return err
		}
	case "MD5SUM":
		entry.Md5, err = parseStr(value)
		if err != nil {
			return err
		}
	case "SHA256SUM":
		entry.Sha256, err = parseStr(value)
		if err != nil {
			return err
		}
	case "PGPSIG":
		entry.PgpSig, err = parseStr(value)
		if err != nil {
			return err
		}
	case "URL":
		entry.Url, err = parseStr(value)
		if err != nil {
			return err
		}
	case "LICENSE":
		entry.Licence, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "ARCH":
		entry.Arch, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "BUILDDATE":
		entry.BuildDate, err = parseUint(value)
		if err != nil {
			return err
		}
	case "PACKAGER":
		entry.Packager, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "REPLACES":
		entry.Replaces, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "CONFLICTS":
		entry.Conflicts, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "PROVIDES":
		entry.Provides, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "DEPENDS":
		entry.Depends, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "OPTDEPENDS":
		entry.OptDepends, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "MAKEDEPENDS":
		entry.MakeDepends, err = parseStrList(value)
		if err != nil {
			return err
		}
	case "CHECKDEPENDS":
		entry.CheckDepends, err = parseStrList(value)
		if err != nil {
			return err
		}
	default:
		return ErrUnknownKey{Key: key}
	}
	return nil
}

func parseField(data []byte) (string, []byte, error) {
	var key string
	var otherData []byte
	if len(data) < 4 {
		return key, otherData, ErrNotParsable{Data: data, Reason: "no data"}
	}
	split := strings.SplitN(string(data), "%", 3)
	if len(split) != 3 {
		return key, otherData, ErrNotParsable{Data: data, Reason: "unexpected data"}
	}
	return split[1], data[len(split[1])+len(split[0])+2:], nil
}

func parseStr(value []byte) (string, error) {
	return strings.Trim(string(value), "\n"), nil
}

func parseUint(value []byte) (uint, error) {
	strVal := strings.Trim(string(value), "\n")
	v, err := strconv.Atoi(strVal)
	if err != nil {
		return 0, err
	}
	if v < 0 {
		return 0, ErrNotUnsigned
	}
	return uint(v), nil
}

func parseStrList(value []byte) ([]string, error) {
	var result []string
	split := strings.Split(string(value), "\n")
	for _, v := range split {
		if v != "\n" {
			result = append(result, v)
		}
	}
	return result, nil
}
