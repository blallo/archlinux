package archlinux

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"
)

const baseRepoSearchUrl = "https://archlinux.org/packages/search/json/"

type SearchResult struct {
	Version uint       `json:"version"`
	Limit   uint       `json:"limit"`
	Valid   bool       `json:"valid"`
	Results []*PkgInfo `json:"results"`
}

type JSONURL struct {
	url.URL
}

func (u *JSONURL) UnmarshalJSON(data []byte) error {
	var raw string

	err := json.Unmarshal(data, &raw)
	if err != nil {
		return err
	}

	parsed, err := url.Parse(raw)
	if err != nil {
		return err
	}
	u.URL = *parsed
	return nil
}

type JSONTime struct {
	time.Time
}

func (t *JSONTime) UnmarshalJSON(data []byte) error {
	var raw string

	err := json.Unmarshal(data, &raw)
	if err != nil {
		return err
	}

	parsed, err := time.Parse(time.RFC3339, raw)
	if err != nil {
		return err
	}
	t.Time = parsed
	return nil
}

type PkgInfo struct {
	Name           string    `json:"pkgname"`
	Base           string    `json:"pkgbase"`
	Repo           string    `json:"repo"`
	Arch           string    `json:"arch"`
	Version        string    `json:"pkgver"`
	Release        string    `json:"pkgrel"`
	Epoch          uint      `json:"epoch"`
	Description    string    `json:"pkgdesc"`
	Url            *JSONURL  `json:"url"`
	Filename       string    `json:"filename"`
	CompressedSize uint      `json:"compressed_size"`
	InstalledSize  uint      `json:"installed_size"`
	BuildDate      *JSONTime `json:"build_date"`
	LastUpdate     *JSONTime `json:"last_update"`
	FlagDate       *JSONTime `json:"flag_date"`
	Maintainers    []string  `json:"maintainers"`
	Packager       string    `json:"packager"`
	Groups         []string  `json:"groups"`
	Licences       []string  `json:"licences"`
	Conflicts      []string  `json:"conflicts"`
	Provides       []string  `json:"provides"`
	Replaces       []string  `json:"replaces"`
	Depends        []string  `json:"depends"`
	OptDepends     []string  `json:"optdepends"`
	MakeDepends    []string  `json:"makedepends"`
	CheckDepends   []string  `json:"checkdepends"`
}

func (i *PkgInfo) String() string {
	return fmt.Sprint(*i)
}

func getJson(client *http.Client, url string, target interface{}) error {
	r, err := client.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(target)
}

func getJsonWithRetry(
	client *http.Client,
	url string,
	target interface{},
	opts *searchReposOpts,
) error {
	if opts.counter > 0 {
		if err := getJson(client, url, target); err != nil {
			opts.Sleep()
			opts.counter -= 1
			return getJsonWithRetry(client, url, target, opts)
		}
	}
	return getJson(client, url, target)
}

func SearchRepos(client *http.Client, name string, opts *searchReposOpts) (SearchResult, error) {
	var empty, result SearchResult
	var queryStr, query string
	if opts.Exact {
		query = "name"
	} else {
		query = "q"
	}
	queryStr = fmt.Sprintf("%s?%s=%s", baseRepoSearchUrl, query, name)
	err := getJsonWithRetry(client, queryStr, &result, opts)
	if err != nil {
		return empty, err
	}
	return result, nil
}

type searchReposOpts struct {
	Exact   bool
	Retry   int
	counter int
}

func (o *searchReposOpts) Sleep() {
	howMuchTime := int64(math.Pow10(o.Retry-o.counter+1)) * int64(time.Millisecond)
	fmt.Println("Sleeping:", howMuchTime/1_000_000, "ms")
	time.Sleep(time.Duration(howMuchTime))
}

func SearchReposOpts(exact bool, retry int) *searchReposOpts {
	if retry < 0 {
		retry = 0
	}
	return &searchReposOpts{Exact: exact, Retry: retry, counter: retry}
}

type SnoopingTransport struct {
	transport http.RoundTripper
	funnel    chan []byte
}

func (s *SnoopingTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	buf, err := httputil.DumpRequest(r, true)
	if err != nil {
		return nil, err
	}

	resp, err := s.transport.RoundTrip(r)
	if err != nil {
		return nil, err
	}

	buf, err = httputil.DumpResponse(resp, true)
	if err != nil {
		return nil, err
	}

	go func() {
		s.funnel <- buf
	}()
	return resp, nil
}

func NewSnoopingClient(funnel chan []byte) *http.Client {
	var client = &http.Client{Timeout: 10 * time.Second}
	client.Transport = &SnoopingTransport{funnel: funnel, transport: http.DefaultTransport}
	return client
}
