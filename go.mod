module git.autistici.org/blallo/archlinux

go 1.15

require (
	github.com/google/go-cmp v0.5.4
	github.com/mikkeloscar/gopkgbuild v0.0.0-20201010175455-2582c34596c6
)
