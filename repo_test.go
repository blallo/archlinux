package archlinux

import (
	"encoding/json"
	"net/url"
	"testing"
	"time"
)

func TestUnmarshal(t *testing.T) {
	var result SearchResult
	err := json.Unmarshal([]byte(resultData), &result)
	if err != nil {
		t.Error(err)
	}

	if len(result.Results) != 2 {
		t.Error("unexpected results")
	}

	if result.Results[0].Name != "nmap" || result.Results[1].Name != "vulscan" {
		t.Error("unexpected package")
	}

	parsedUrl, _ := url.Parse("https://nmap.org/")
	if result.Results[0].Url.URL != *parsedUrl {
		t.Error("unexpected url")
	}

	parsedTime, _ := time.Parse(time.RFC3339, "2020-07-07T17:03:08Z")
	if result.Results[1].BuildDate.Time != parsedTime {
		t.Error("unexpected time")
	}
}

var resultData = `{"version": 2, "limit": 250, "valid": true, "results": [{"pkgname": "nmap", "pkgbase": "nmap", "repo": "extra", "arch": "x86_64", "pkgver": "7.91", "pkgrel": "1", "epoch": 0, "pkgdesc": "Utility for network discovery and security auditing", "url": "https://nmap.org/", "filename": "nmap-7.91-1-x86_64.pkg.tar.zst", "compressed_size": 5676162, "installed_size": 25169503, "build_date": "2020-11-10T22:07:21Z", "last_update": "2020-11-10T22:28:41.173Z", "flag_date": null, "maintainers": ["anthraxx"], "packager": "anthraxx", "groups": [], "licenses": ["GPL2"], "conflicts": [], "provides": [], "replaces": [], "depends": ["gcc-libs", "glibc", "libpcap", "libssh2", "libssh2.so=1-64", "lua53", "openssl", "pcre", "zlib"], "optdepends": [], "makedepends": [], "checkdepends": []}, {"pkgname": "vulscan", "pkgbase": "vulscan", "repo": "community", "arch": "any", "pkgver": "2.0", "pkgrel": "4", "epoch": 0, "pkgdesc": "A module which enhances nmap to a vulnerability scanner", "url": "https://www.computec.ch/projekte/vulscan/", "filename": "vulscan-2.0-4-any.pkg.tar.zst", "compressed_size": 5933225, "installed_size": 38364620, "build_date": "2020-07-07T17:03:08Z", "last_update": "2020-07-09T18:08:08.053Z", "flag_date": "2020-02-18T16:46:42.885Z", "maintainers": ["anthraxx"], "packager": "felixonmars", "groups": [], "licenses": ["GPL"], "conflicts": ["nmap-vulscan"], "provides": ["nmap-vulscan"], "replaces": ["nmap-vulscan"], "depends": ["nmap"], "optdepends": [], "makedepends": [], "checkdepends": []}], "num_pages": 1, "page": 1}`
