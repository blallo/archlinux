package archlinux

import (
	"fmt"
	"strings"

	"github.com/mikkeloscar/gopkgbuild"
)

type source int

const (
	SourceBuiltin source = iota
	SourceRepo
	SourceAUR
)

type Dep struct {
	Name      string
	Pkg       *pkgbuild.Dependency
	IsVirt    bool
	Providers []*pkgbuild.Dependency
}

func (d *Dep) String() string {
	if d.IsVirt {
		providers := ""
		for _, pkg := range d.Providers {
			providers += fmt.Sprintf("%s|", pkg)
		}
		providers = strings.TrimRight(providers, "|")
		return fmt.Sprintf("virtual(%s -> %s)", d.Name, providers)
	}
	return d.Name
}

type DepNode struct {
	Pkg       *Dep
	NeededBy  *pkgbuild.Dependency
	Deps      DepList
	BuildDeps DepList
	Source    source
}

func (dn *DepNode) String() string {
	return dn.Pkg.String()
}

func (dn *DepNode) GetPkg() *pkgbuild.Dependency {
	return dn.Pkg.Pkg
}

func (dn *DepNode) WithPkg(pkg *Dep) *DepNode {
	dn.Pkg = pkg
	return dn
}

func NewNode(name string) *DepNode {
	return &DepNode{Pkg: &Dep{Name: name}}
}

type DepTree struct {
	RootPkg   string
	Deps      DepList
	BuildDeps DepList
	Source    source
}

type DepList []*DepNode

func (dl DepList) String() string {
	out := "DepList{"
	for _, node := range dl {
		out += fmt.Sprintf("%s, ", node.Pkg.Name)
	}
	out = strings.TrimRight(out, ", ")
	out += "}"
	return out
}

func (dl DepList) SimpleList() []string {
	var out []string
	for _, node := range dl {
		out = append(out, node.Pkg.Name)
	}
	return out
}

func (t *DepTree) FlattenDeps() DepList {
	return appendDepsNode([]*DepNode{}, t.Deps)
}

func appendDepsNode(acc []*DepNode, nodes []*DepNode) DepList {
	acc = append(acc, nodes...)
	for _, dep := range nodes {
		acc = appendDepsNode(acc, dep.Deps)
	}
	return acc
}

func (t *DepTree) FlattenAll() DepList {
	return appendNode([]*DepNode{}, t.Deps, t.BuildDeps)
}

func appendNode(acc []*DepNode, nodes, buildNodes []*DepNode) DepList {
	acc = append(acc, nodes...)
	for _, dep := range buildNodes {
		if absentAtFirstLevel(dep, acc) {
			acc = append(acc, dep)
		}
	}
	for _, dep := range append(nodes, buildNodes...) {
		acc = appendNode(acc, dep.Deps, dep.BuildDeps)
	}
	return acc
}

func absentAtFirstLevel(node *DepNode, acc []*DepNode) bool {
	for _, curNode := range acc {
		if curNode.Pkg == node.Pkg {
			return false
		}
	}
	return true
}
