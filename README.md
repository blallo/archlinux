What?
=====


This is a package containing utilities to deal with Arch Linux packaging machinery.

How?
====

```
go get -u git.autistici.org/blallo/archlinux
```

Why?
====

I use Arch Linux and would like to tinker with it in go.

Contact
=======

You can email me at `blallo -|AT|- autistici[.]org`
